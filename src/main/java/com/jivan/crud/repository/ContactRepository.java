package com.jivan.crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.jivan.crud.pojo.Contact;

public interface ContactRepository extends CrudRepository<Contact,String>{
	
	@Query(value = "SELECT * FROM CONTACT WHERE CITY = ?1" ,nativeQuery=true)
    public Iterable<Contact> findByCity(String city);
	
	@Query(value = "SELECT * FROM CONTACT WHERE STATE = ?1" ,nativeQuery=true)
    public Iterable<Contact> findByState(String state);
	
	@Query(value = "SELECT * FROM CONTACT WHERE EMAIL = ?1" ,nativeQuery=true)
    public Iterable<Contact> findByEmail(String email);
	
	@Query(value = "SELECT * FROM CONTACT WHERE PHONE = ?1" ,nativeQuery=true)
    public Iterable<Contact> findByPhone(String phone);
	
}
