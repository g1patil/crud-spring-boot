package com.jivan.crud.service;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import javax.activation.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jivan.crud.pojo.Contact;
import com.jivan.crud.repository.ContactRepository;

@Service
public class CrudService {
	
	@Autowired
	ContactRepository contactRepository;
	
	public boolean createOrUpdateContact(Contact contact){
		contactRepository.save(contact);
		return true;
	}
	
	public boolean deleteContact(Contact contact){
		
		if(!recordValidation(contact))
			return false;
		
		contactRepository.delete(contact);
		return true;
	}
	
	public List<Contact> getAllContacts(){
		List<Contact> allContacts = new ArrayList<>();
		contactRepository.findAll().forEach(allContacts::add);
		return allContacts;
	}
	
	public Contact getContactForEmail(String email) throws NoSuchElementException{
		Iterator<Contact> i= contactRepository.findByEmail(email).iterator();
		return i.hasNext() ? i.next() : null;
	}
	
	public Contact getContactForPhone(String phone){
		Iterator<Contact> i= contactRepository.findByPhone(phone).iterator();
		return i.hasNext() ? i.next() : null;
	}
	
	public List<Contact> getContactForState(String state){
		List<Contact> allContacts = new ArrayList<>();
		contactRepository.findByState(state).forEach(allContacts::add);
		return allContacts;

	}
	
	public List<Contact> getContactForCity(String city){
		List<Contact> allContacts = new ArrayList<>();
		contactRepository.findByCity(city).forEach(allContacts::add);
		return allContacts;

	}

	public boolean recordValidation(Contact contact){
		if(contactRepository.findByEmail(contact.getEmail()).iterator().hasNext())
			return true;
		return false;
	}
	
}
