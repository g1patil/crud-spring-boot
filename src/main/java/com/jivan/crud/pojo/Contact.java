package com.jivan.crud.pojo;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="CONTACT")
public class Contact {
	
	@Id
	private String email;
	private String name;
	private String company;
	private String phone;
	private String profileImage;
	@Embedded
	private Address address;
	
	
	public Contact(String email, String name, String company, String phone, String profileImage, Address address) {
		super();
		this.email = email;
		this.name = name;
		this.company = company;
		this.phone = phone;
		this.profileImage = profileImage;
		this.address = address;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Contact(){}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getProfileImage() {
		return profileImage;
	}
	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}
	
}
