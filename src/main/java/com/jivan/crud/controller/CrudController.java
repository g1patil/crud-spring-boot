package com.jivan.crud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jivan.crud.pojo.Contact;
import com.jivan.crud.service.CrudService;

@RestController
public class CrudController {
	
	@Autowired
	CrudService crudService;
	
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public List<Contact> getAllRecord(){
		return crudService.getAllContacts();
	}
	
	@RequestMapping(value="/contact/email/{email}", method=RequestMethod.GET)
	public Contact getContactForEmail(@PathVariable String email){
		return crudService.getContactForEmail(email);
	}
	
	@RequestMapping(value="/contact/phone/{phone}", method=RequestMethod.GET)
	public Contact getContactForPhone(@PathVariable String phone){
		return crudService.getContactForPhone(phone);
	}
	
	@RequestMapping(value="/contact/state/{state}", method=RequestMethod.GET)
	public List<Contact> getContactForState(@PathVariable String state){
		return crudService.getContactForState(state);
	}
	
	@RequestMapping(value="/contact/city/{city}", method=RequestMethod.GET)
	public List<Contact> getContactForCity(@PathVariable String city){
		return crudService.getContactForCity(city);
	}
	
	@RequestMapping(value="/create", method=RequestMethod.POST)
	public String createRecord(@RequestBody Contact contact){
		if(crudService.createOrUpdateContact(contact))
			return "Record successfully created";
		return "Record for email : " + contact.getEmail() + "already exist ";
	}
	
	@RequestMapping(value="/update", method=RequestMethod.PUT)
	public String updateRecord(@RequestBody Contact contact){
		if(crudService.createOrUpdateContact(contact))
			return "Record updated successfully";
		return "Record does not exist";
		
	}
	
	@RequestMapping(value="/delete", method=RequestMethod.DELETE)
	public String deleteRecord(@RequestBody Contact contact){
		if(crudService.deleteContact(contact))
			return "Record deleted successfully ";
		return "Record does not exixts for email";
	}
}
