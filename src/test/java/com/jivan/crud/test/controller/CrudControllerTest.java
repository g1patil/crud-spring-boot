package com.jivan.crud.test.controller;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.jivan.crud.controller.CrudController;
import com.jivan.crud.pojo.Address;
import com.jivan.crud.pojo.Contact;
import com.jivan.crud.service.CrudService;

@RunWith(SpringRunner.class)
@WebMvcTest(CrudController.class)
@AutoConfigureMockMvc(secure=false)
public class CrudControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private CrudService crudService;
	
	Address address=new Address("line1", "city", "state");
	Contact mockContact=new Contact("email1", "name1", "company1",
									"phone", "profileImage", address);
	
	@Test
	public void getContactForEmailTest() throws Exception{
		Mockito.when(crudService.getContactForEmail(Mockito.anyString())).thenReturn(mockContact);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/contact/email/email 1")
										.accept(MediaType.APPLICATION_JSON);
		MvcResult response = mockMvc.perform(requestBuilder).andReturn();
		JSONAssert.assertEquals("{\"email\":\"email1\",\"name\":\"name1\",\"company\":"
				+ "\"company1\",\"phone\":\"phone\",\"profileImage\":\"profileImage\","
				+ "\"address\":{\"line1\":\"line1\",\"city\":\"city\",\"state\":\"state\"}}",
				response.getResponse().getContentAsString(),false);
	}
	
}

