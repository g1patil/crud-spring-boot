This is Crud REST application project using Spring Boot and in memory h2 database. Please follow steps for running project. Use end point and sample json object where necessary.  
I have implemented Spring security for delete method. Use username/password given. Chhose basic authentication in Postman client and put login details.


### How do I get set up? ###

* Pull the code in local machine
* If using eclipse (import project in IDE as Maven project)
* Right click and update maven project
* Right click on MyApplication.java (Run as Java Application)
* Open link localhost:8080

* For other IntelliJ -> Import as a maven project and run MyApplication.Java
### running using command line
* Download maven and set it in classpath. Follow link https://www.mkyong.com/maven/how-to-install-maven-in-windows/ for steps.
* Set java classpath

run command: go to project directory
  mvn clean install
  java -jar target/CrudApplicationSpringBoot-0.0.1-SNAPSHOT.jar
### API endpoints

* localhost:8080/create                     POST
* localhost:8080/update                     PUT
* localhost:8080/contact/city/{cityname}    GET
* localhost:8080/contact/state/{statename}  GET
* localhost:8080/delete                     DELETE
delete needs basic Authentication

  Basic Authentication
  username: admin
  password: admin

### Sample JSON for create,update,delete

* {
        "email": "email 4",
        "name": "manoj",
        "company": "c1",
        "phone": null,
        "profileImage": null,
        "date" : "1990-01-01",
        "address": {
            "line1": "l1",
            "city": "c1",
            "state": "il"
        }
}
